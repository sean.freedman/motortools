from click import option
import streamlit as st

import os
#import pages.encoder
# OPEN CMD: streamlit run <filename>.py

st.title("Setup")

st.warning("Currently Under development, Using default 340kW Preset (i.e DCDC config = Parallel)")
st.markdown("Using the options below, chose the relevent options. This will configure various settings regarding the test equipment.")

@st.cache(allow_output_mutation=True)
def intialise():
    projects = [
        "Derwent",
        "Bowfell",
        "Oxford",
        "Other"
    ]

    dynoLocations = [
        "DX: 340kW",
        "DX: 160kW",
        "DX: 100kW"
    ]

    initialSetup = {
        "loggingPath"       : os.getcwd()+"\Log",
        "project"           : 'Derwent',
        "dynoGear"          : 0.0,
        "dynoLocation"      : dynoLocations[0],
        "dynoIP"            : "",
        "ixxatMCU"          : "",
        "ixxatDCDC"         : "",
        "dcdcConfig"        : "",
        "setupDefaults"     : True

        }

    initialSafteyMonitorDyno = {
            "speedMin"      : -100.0,
            "speedMax"      : 500.0,
            "vdcFlag"       : True,
            "AccMin"        : 0,
            "AccMax"        : 90,
            "AccFlag"       : True,
            "tempIGBTMin"   : 0.0,
            "tempIGBTMax"   : 120.0,
            "tempIGBTFlag"  : True,
        }

    initialSafteyMonitorDCDC = {
            "vdcMin"        : -100.0,
            "vdcMax"        : 500.0,
            "vdcFlag"       : True,
            "tempIGBTMin"   : 0.0,
            "tempIGBTMax"   : 120.0,
            "tempIGBTFlag"  : True,
        }

    initialSafteyMonitorMCU = {
        "vdcMin"            : -10.0,
        "vdcMax"            : 500.0,
        "vdcFlag"           : True,
        "idcMin"            : -500.0,
        "idcMax"            : 500.0,
        "idcFlag"           : True,
        "speedMin"          : -5000.0,
        "speedMax"          : 5000.0,
        "speedFlag"         : True,
        "torqueMin"         : -1000.0,
        "torqueMax"         : 1000.0,
        "torqueFlag"        : True,
        "tempMotorMin"      : 0.0,
        "tempMotorMax"      : 120.0,
        "tempMotorFlag"     : True,
        "tempMCUMin"        : 0.0,
        "tempMCUMax"        : 120.0,
        "tempMCUFlag"       : True,
        "tempIGBTMin"       : 0.0,
        "tempIGBTMax"       : 120.0,
        "tempIGBTFlag"      : True,
    }

    initialAlignEncoder = {
        "test"                              : {
            "DCDC"                          : {
                "voltageTarget"             : 0.0,
                "currentLimPos"             : 500.0,
                "currentLimNeg"             : -500.0,
            },
            "MCU"                           : {
                "currentLimPos"             : 400.0,
                "currentLimNeg"             : -400.0,
            },
            "encoderAngleOffsetID"          : "0x20180015",
            "method"                        : "",
            "udq"                           : {
                "targetModIndex"            : 60,
                "mcuForwardSpeedLimit"      : 1000,
                "mcuReverseSpeedLimit"      : -1000,          
            },
            "idTorque"                      : {
                "targetIdCurrent"           : 0.0,         
                },
        }
    }
    
    initialAlignEncoderTable = {
        "test"                              : {
            "DCDC"                          : {
                "voltageTarget"             : 0.0,
                "currentLimPos"             : 500.0,
                "currentLimNeg"             : -500.0,
            },
            "MCU"                           : {
                "currentLimPos"             : 400.0,
                "currentLimNeg"             : -400.0,
            },
            "targetModIndex"                : 80,
            "encoderTrimTableID"            : "0x20180016",
        },
    }
        
    return projects, dynoLocations, initialSetup, initialSafteyMonitorDyno, initialSafteyMonitorDCDC, initialSafteyMonitorMCU, initialAlignEncoder,initialAlignEncoderTable


def dictToSessionState(sub_dictionary_name, input_dictionary):
    if sub_dictionary_name not in st.session_state:
        st.session_state[str(sub_dictionary_name)] = dict()

    for i in range(0, len(input_dictionary)):
        if list(input_dictionary.keys())[i] not in st.session_state[str(sub_dictionary_name)]:
            st.session_state[str(sub_dictionary_name)][ str(list(input_dictionary.keys())[i]) ] = list(input_dictionary.values())[i]


# Intiailsation
projects, dynoLocations, initialSetup, initialSafteyMonitorDyno, initialSafteyMonitorDCDC, initialSafteyMonitorMCU, initialAlignEncoder, initialAlignEncoderTable = intialise() 

dictToSessionState("setup", initialSetup)
dictToSessionState("safteyMonitorDyno", initialSafteyMonitorDyno)
dictToSessionState("safteyMonitorDCDC", initialSafteyMonitorDCDC)
dictToSessionState("safteyMonitorMCU", initialSafteyMonitorMCU)
dictToSessionState("alignEncoder", initialAlignEncoder)
dictToSessionState("alignEncoderTable", initialAlignEncoderTable)

st.sidebar.subheader("Setup")

def updateProjectDefaults(project, project_list):
    
    if project == project_list[0]:
        st.session_state["safteyMonitorMCU"]["vdcMin"]          = -10.0
        st.session_state["safteyMonitorMCU"]["vdcMax"]          = 500.0
        st.session_state["safteyMonitorMCU"]["vdcFlag"]         = True
        st.session_state["safteyMonitorMCU"]["idcMin"]          = -500.0
        st.session_state["safteyMonitorMCU"]["idcMax"]          = 500.0
        st.session_state["safteyMonitorMCU"]["idcFlag"]         = True
        st.session_state["safteyMonitorMCU"]["speedMin"]        = -5000.0
        st.session_state["safteyMonitorMCU"]["speedMax"]        = 5000.0
        st.session_state["safteyMonitorMCU"]["speedFlag"]       = True
        st.session_state["safteyMonitorMCU"]["torqueMin"]       = -1000.0
        st.session_state["safteyMonitorMCU"]["torqueMax"]       = 1000.0
        st.session_state["safteyMonitorMCU"]["torqueFlag"]      = True
        st.session_state["safteyMonitorMCU"]["tempMotorMin"]    = 0.0
        st.session_state["safteyMonitorMCU"]["tempMotorMax"]    = 120.0
        st.session_state["safteyMonitorMCU"]["tempMotorFlag"]   = True
        st.session_state["safteyMonitorMCU"]["tempMCUMin"]      = 0.0
        st.session_state["safteyMonitorMCU"]["tempMCUMax"]      = 120.0
        st.session_state["safteyMonitorMCU"]["tempMCUFlag"]     = True
        st.session_state["safteyMonitorMCU"]["tempIGBTMin"]     = 0.0
        st.session_state["safteyMonitorMCU"]["tempIGBTMax"]     = 120.0
        st.session_state["safteyMonitorMCU"]["tempIGBTFlag"]    = True

    elif project == project_list[1]:
        st.session_state["safteyMonitorMCU"]["vdcMin"]          = -10.0
        st.session_state["safteyMonitorMCU"]["vdcMax"]          = 420.0
        st.session_state["safteyMonitorMCU"]["vdcFlag"]         = True
        st.session_state["safteyMonitorMCU"]["idcMin"]          = -500.0
        st.session_state["safteyMonitorMCU"]["idcMax"]          = 500.0
        st.session_state["safteyMonitorMCU"]["idcFlag"]         = True
        st.session_state["safteyMonitorMCU"]["speedMin"]        = -12000.0
        st.session_state["safteyMonitorMCU"]["speedMax"]        = 12000.0
        st.session_state["safteyMonitorMCU"]["speedFlag"]       = True
        st.session_state["safteyMonitorMCU"]["torqueMin"]       = -300.0
        st.session_state["safteyMonitorMCU"]["torqueMax"]       = 300.0
        st.session_state["safteyMonitorMCU"]["torqueFlag"]      = True
        st.session_state["safteyMonitorMCU"]["tempMotorMin"]    = 0.0
        st.session_state["safteyMonitorMCU"]["tempMotorMax"]    = 120.0
        st.session_state["safteyMonitorMCU"]["tempMotorFlag"]   = True
        st.session_state["safteyMonitorMCU"]["tempMCUMin"]      = 0.0
        st.session_state["safteyMonitorMCU"]["tempMCUMax"]      = 120.0
        st.session_state["safteyMonitorMCU"]["tempMCUFlag"]     = True
        st.session_state["safteyMonitorMCU"]["tempIGBTMin"]     = 0.0
        st.session_state["safteyMonitorMCU"]["tempIGBTMin"]     = 120.0
        st.session_state["safteyMonitorMCU"]["tempIGBTFlag"]    = True

    elif project == project_list[2]:
        st.session_state["safteyMonitorMCU"]["vdcMin"]          = -10.0
        st.session_state["safteyMonitorMCU"]["vdcMax"]          = 380.0
        st.session_state["safteyMonitorMCU"]["vdcFlag"]         = True
        st.session_state["safteyMonitorMCU"]["idcMin"]          = -500.0
        st.session_state["safteyMonitorMCU"]["idcMax"]          = 500.0
        st.session_state["safteyMonitorMCU"]["idcFlag"]         = True
        st.session_state["safteyMonitorMCU"]["speedMin"]        = -9000.0
        st.session_state["safteyMonitorMCU"]["speedMax"]        = 9000.0
        st.session_state["safteyMonitorMCU"]["speedFlag"]       = True
        st.session_state["safteyMonitorMCU"]["torqueMin"]       = -290.0
        st.session_state["safteyMonitorMCU"]["torqueMax"]       = 290.0
        st.session_state["safteyMonitorMCU"]["torqueFlag"]      = True
        st.session_state["safteyMonitorMCU"]["tempMotorMin"]    = 0.0
        st.session_state["safteyMonitorMCU"]["tempMotorMax"]    = 120.0
        st.session_state["safteyMonitorMCU"]["tempMotorFlag"]   = True
        st.session_state["safteyMonitorMCU"]["tempMCUMin"]      = 0.0
        st.session_state["safteyMonitorMCU"]["tempMCUMax"]      = 120.0
        st.session_state["safteyMonitorMCU"]["tempMCUFlag"]     = True
        st.session_state["safteyMonitorMCU"]["tempIGBTMin"]     = 0.0
        st.session_state["safteyMonitorMCU"]["tempIGBTMin"]     = 120.0
        st.session_state["safteyMonitorMCU"]["tempIGBTFlag"]    = True

def updateLocationDefaults():
    
    if st.session_state["setup"]["dynoLocation"] == dynoLocations[0] :
        
        # Setup 
        st.session_state["setup"]["dynoIP"]                     = "192.168.1.2"
        st.session_state["setup"]["dynoGear"]                   = 3.06
        st.session_state["setup"]["ixxatMCU"]                   = "HW507598"
        st.session_state["setup"]["ixxatDCDC"]                  = "HW484965"
        st.session_state["setup"]["dcdcConfig"]                 = "Parallel"
        # Saftey Monitor Dyno Defaults
        st.session_state["safteyMonitorDyno"]["speedMin"]       = -100.0
        st.session_state["safteyMonitorDyno"]["speedMax"]       = 500.0
        st.session_state["safteyMonitorDyno"]["vdcFlag"]        = True
        st.session_state["safteyMonitorDyno"]["AccMin"]         = 0.0
        st.session_state["safteyMonitorDyno"]["AccMax"]         = 90.0
        st.session_state["safteyMonitorDyno"]["AccFlag"]        = True
        st.session_state["safteyMonitorDyno"]["tempIGBTMin"]    = 0.0
        st.session_state["safteyMonitorDyno"]["tempIGBTMax"]    = 120.0
        st.session_state["safteyMonitorDyno"]["tempIGBTFlag"]   = True
        # Saftey Monitor DCDC Defaults
        st.session_state["safteyMonitorDCDC"]["vdcMin"]         = -10.0
        st.session_state["safteyMonitorDCDC"]["vdcMax"]         = 500.0
        st.session_state["safteyMonitorDCDC"]["vdcFlag"]        = True
        st.session_state["safteyMonitorDCDC"]["tempIGBTMin"]    = 0.0
        st.session_state["safteyMonitorDCDC"]["tempIGBTMax"]    = 120.0
        st.session_state["safteyMonitorDCDC"]["tempIGBTFlag"]   = True

    elif st.session_state["setup"]["dynoLocation"] == dynoLocations[1]:
        
        st.session_state["setup"]["dynoIP"]                     = "192.168.1.1"
        st.session_state["setup"]["dynoGear"]                   = 3.06
        st.session_state["setup"]["ixxatMCU"]                   = "HW111111"
        st.session_state["setup"]["ixxatDCDC"]                  = "HW121111"
        st.session_state["setup"]["dcdcConfig"]                 = "Single"
        # Saftey Monitor Dyno Defaults
        st.session_state["safteyMonitorDyno"]["speedMin"]       = -100.0
        st.session_state["safteyMonitorDyno"]["speedMax"]       = 500.0
        st.session_state["safteyMonitorDyno"]["vdcFlag"]        = True
        st.session_state["safteyMonitorDyno"]["AccMin"]         = 0.0
        st.session_state["safteyMonitorDyno"]["AccMax"]         = 90.0
        st.session_state["safteyMonitorDyno"]["AccFlag"]        = True
        st.session_state["safteyMonitorDyno"]["tempIGBTMin"]    = 0.0
        st.session_state["safteyMonitorDyno"]["tempIGBTMax"]    = 120.0
        st.session_state["safteyMonitorDyno"]["tempIGBTFlag"]   = True
        # Saftey Monitor DCDC Defaults
        st.session_state["safteyMonitorDCDC"]["vdcMin"]         = -10
        st.session_state["safteyMonitorDCDC"]["vdcMax"]         = 500
        st.session_state["safteyMonitorDCDC"]["vdcFlag"]        = True
        st.session_state["safteyMonitorDCDC"]["tempIGBTMin"]    = 0.0
        st.session_state["safteyMonitorDCDC"]["tempIGBTMax"]    = 120.0
        st.session_state["safteyMonitorDCDC"]["tempIGBTFlag"]   = True

    elif st.session_state["setup"]["dynoLocation"] == dynoLocations[2] :
        
        st.session_state["setup"]["dynoIP"]                     = "192.168.1.67"
        st.session_state["setup"]["dynoGear"]                   = 3.00
        st.session_state["setup"]["ixxatMCU"]                   = "HW222222"
        st.session_state["setup"]["ixxatDCDC"]                  = "HW232222"
        st.session_state["setup"]["dcdcConfig"]                 = "Parallel"
        # Saftey Monitor Dyno Defaults
        st.session_state["safteyMonitorDyno"]["speedMin"]       = -100.0
        st.session_state["safteyMonitorDyno"]["speedMax"]       = 500.0
        st.session_state["safteyMonitorDyno"]["vdcFlag"]        = True
        st.session_state["safteyMonitorDyno"]["AccMin"]         = 0.0
        st.session_state["safteyMonitorDyno"]["AccMax"]         = 90.0
        st.session_state["safteyMonitorDyno"]["AccFlag"]        = True
        st.session_state["safteyMonitorDyno"]["tempIGBTMin"]    = 0.0
        st.session_state["safteyMonitorDyno"]["tempIGBTMax"]    = 120.0
        st.session_state["safteyMonitorDyno"]["tempIGBTFlag"]   = True
        # Saftey Monitor DCDC Defaults
        st.session_state["safteyMonitorDCDC"]["vdcMin"]         = -10.0
        st.session_state["safteyMonitorDCDC"]["vdcMax"]         = 500.0
        st.session_state["safteyMonitorDCDC"]["vdcFlag"]        = True
        st.session_state["safteyMonitorDCDC"]["tempIGBTMin"]    = 0.0
        st.session_state["safteyMonitorDCDC"]["tempIGBTMax"]    = 120.0
        st.session_state["safteyMonitorDCDC"]["tempIGBTFlag"]   = True

def Setup():
    with st.expander("Inverter", expanded=True):
        #st.session_state["setup"]["project"]            = st.selectbox("Project", projects, on_change = updateProjectDefaults(st.session_state["setup"]["project"], projects))
        st.session_state["setup"]["project"]            = st.radio("Project", options=projects, on_change=updateProjectDefaults(st.session_state["setup"]["project"], projects), disabled=True, key = "Project" )

    with st.expander("Dyno", expanded=True):
        st.session_state["setup"]["dynoLocation"]       = st.radio("Location", options = dynoLocations, disabled= True, key = "Location"  )
        st.session_state["setup"]["dynoGear"]           = st.number_input("Gear Ratio", min_value = 0.00, max_value = 10.00, value = st.session_state["setup"]["dynoGear"], step=0.01)

        st.session_state["setup"]["dynoIP"]             = st.text_input("Dyno IP", value = st.session_state["setup"]["dynoIP"])

    with  st.expander("CAN Hardware" ,expanded=True):
        st.session_state["setup"]["ixxatMCU"]           = st.text_input("MCU Ixxat HWID", value = st.session_state["setup"]["ixxatMCU"])
        st.session_state["setup"]["ixxatDCDC"]          = st.text_input("DCDC Ixxat HWID", value = st.session_state["setup"]["ixxatDCDC"])
        st.session_state["setup"]["loggingPath"]        = st.text_input("Log Directory", value = st.session_state["setup"]["loggingPath"])
        
Setup()