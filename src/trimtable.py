import numpy as np
import pandas as pd
import plotly.graph_objects as go
import time
import tkinter.messagebox as mb
from MunchPy import *

# ENCODER TRIM TABLE FUNCTIONS
def getEncoderTrimTable(alignEncoder):
    munchLoggingPath = "."
    munchInterop = MunchInterop.getInstance()

    if munchInterop is not None:
        munchInterop.initialise(munchLoggingPath, False)
        time.sleep(1)

        try:
            mcuCanDevice = CANDevice.getSpecific(munchInterop,  alignEncoder["setup"]["ixxatMCU"])
            mcu = Derwent(munchInterop, mcuCanDevice)

            mb.showinfo("Test Paused", "Turn on KL30 Tenma PSU")

            encoderTrimTable = mcu.readBaseCommsProperty( alignEncoder["setup"]["ixxatMCU"], True)
        except:
            encoderTrimTable = "Error getting encoder Trim Table"

    return encoderTrimTable

def plotEncoderTrimTable(encoderTrimTable):

    return

def getEncoderTrimTableInputs():

    return

def calculateEncoderTrimTable():

    return

def saveEncoderTrimTable():

    return

def checkEncoderTrimTableSave():

    return






# CURRENT TRIM TABLE FUNCTIONS
def getCurrentTrimTable(currentTrimTable, GEN5_CAN_DEVICE):
    munchLoggingPath = "."
    munchInterop = MunchInterop.getInstance()

    if munchInterop is not None:
        munchInterop.initialise(munchLoggingPath, False)
        time.sleep(1)

        try:
            gen5CanDevice = CANDevice.getSpecific(munchInterop, GEN5_CAN_DEVICE)
            gen5 = Derwent(munchInterop, gen5CanDevice)

            mb.showinfo("Test Paused", "Turn on KL30 Tenma PSU")

            currentTrimTable = gen5.readBaseCommsProperty(currentTrimTable, True)
        except:
            currentTrimTable = "Error getting encoder Trim Table"

    return currentTrimTable

def plotCurrentTrimTable():

    return

def getCurrentTrimTableInputs():

    return

def calculateCurrentTrimTable():

    return

def saveCurrentTrimTable():

    return

def checkCurrentTrimTableSave():

    return

