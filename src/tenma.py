import time
import serial
from serial.tools import list_ports

def tenmaVoltageTarget(tenma, v_target):
    tenma.write(str.encode('BEEP1'))
    time.sleep(0.1)
    return tenma.write(str.encode('VSET1:'+str(v_target)))

def tenmaEnableOutput(tenma):
    time.sleep(0.1)
    return tenma.write(str.encode('OUT1'))

def tenmaDisableOutput(tenma):
    time.sleep(0.1)
    return tenma.write(str.encode('OUT0'))

def tenmaGetDevice():
    foundPorts = list(serial.tools.list_ports.comports())

    for port in foundPorts:
        tempPort = serial.Serial()
        tempPort.port = port.device
        tempPort.open()
        
        if tempPort.is_open == True:
            tempPort.write(str.encode('*IDN?'))

            if 'TENMA' in str(tempPort.read(5)):
                tenma = tempPort
                time.sleep(0.1)
    
    return tenma

