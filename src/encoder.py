from binhex import hexbin
import numpy as np
import pandas as pd
import plotly.graph_objects as go
import time
import tkinter.messagebox as mb
from MunchPy import *
import streamlit as st
from src.tenma import tenmaGetDevice, tenmaVoltageTarget, tenmaDisableOutput, tenmaEnableOutput
import seaborn as sns
from sklearn.linear_model import LinearRegression

tenmaVoltage = 16.00

# Functions
def shutdownAll(message):
    
    munchInterop = MunchInterop.getInstance()

    global dcdc
    global gen5
    global dyno
    
    try:
        #dyno.setExternalTrip(True)
        dyno.setPresetReference1(0)
        gen5.setBridgeEnabled(False)
        dcdc.setOutputEnabled(False)
        dyno.setInverterEnabled(False)
        dyno.setDriveEnabled(False)
    finally:
        print(message)
        munchInterop.terminate()

def getCurrentEncoderAlignment(alignEncoder):

    angleRead = 361
    munchLoggingPath = '.'
    munchInterop = MunchInterop.getInstance()

    if munchInterop is not None:
        munchInterop.initialise(munchLoggingPath, True)
        time.sleep(0.2)
        
        try:
            canDevice = CANDevice.getSpecific(munchInterop, alignEncoder["setup"]["ixxatMCU"])
            mcu = Derwent(munchInterop, canDevice)

            try:
                tenma = tenmaGetDevice()
                tenmaVoltageTarget(tenma, tenmaVoltage)
                tenmaEnableOutput(tenma)
#
            except:
                print("Couldn't find Tenma")

            try:
                time.sleep(12)
                mcu.baseCommsLogin()
                
                mcu.readBaseCommsProperty(int(alignEncoder["test"]["encoderAngleOffsetID"], base = 16), True)
                
            except:
                print("Issue retriving Encoder Offset")

            angleRead = mcu.readBaseCommsProperty(int(alignEncoder["test"]["encoderAngleOffsetID"], base = 16), True)
            angleRadians = angleRead
            angleDegrees = angleRead * 360

        except Exception:
            raise

        finally:    
            munchInterop.terminate()

    return angleRadians, angleDegrees

def alignEncoderUdq(alignEncoder, propertyIDs):

    oldEncoderAngleDegrees  = 0
    oldEncoderAngleRadians  = 0
    newEncoderAngleDegrees  = 0
    newEncoderAngleRadians  = 0
    resultUd                = 0
    resultUq                = 0
    resultModIndex          = 0
    resultMUTSpeed          = 0

    # Initialisation
    munchLoggingPath = "."
    munchInterop = MunchInterop.getInstance()

    if munchInterop is not None:

        munchInterop.initialise(munchLoggingPath, True)
        time.sleep(0.5)
    
        try:
            dcdcCanDevice = CANDevice.getSpecific(munchInterop, alignEncoder["setup"]["ixxatDCDC"])
            mcuCanDevice = CANDevice.getSpecific(munchInterop, alignEncoder["setup"]["ixxatMCU"])

            if alignEncoder["setup"]["dcdcConfig"] == "Single":
                dcdc = SingleDCDC(munchInterop, dcdcCanDevice)
            elif alignEncoder["setup"]["dcdcConfig"] == "Parallel":
                dcdc = ParallelDCDC(munchInterop, dcdcCanDevice)  

            if alignEncoder["setup"]["project"] == ("Derwent" or "Bowfell"):
                mcu = Derwent(munchInterop, mcuCanDevice)
            elif alignEncoder["setup"]["project"] == "Oxford":
                mcu = Derwent(munchInterop, mcuCanDevice)

            if alignEncoder["setup"]["dynoLocation"] == ("DX: 340kW" or "DX: 160kW" or "DX: 100kW"):
                dyno = NidecDyno(munchInterop, alignEncoder["setup"]["dynoIP"])
            else:
                dyno = NidecDyno(munchInterop, alignEncoder["setup"]["dynoIP"])
    
            try:
                tenma = tenmaGetDevice()
                tenmaVoltageTarget(tenma, tenmaVoltage)
                tenmaEnableOutput(tenma)

            except:
                print("Couldn't find Tenma")

            # Saftey
            # Optional DCDC safety monitors (Usage strongly advised)
            if alignEncoder["safteyMonitors"]["DCDC"]["vdcFlag"] == True:
                dcdcVoltageMonitor = ValueRangeMonitor(munchInterop, dcdc.getOutputVoltages, shutdownAll, "DCDC Voltages")
                dcdcVoltageMonitor.beginMonitoring(alignEncoder["safteyMonitors"]["DCDC"]["vdcMin"], alignEncoder["safteyMonitors"]["DCDC"]["vdcMax"])
            
            if alignEncoder["safteyMonitors"]["DCDC"]["tempIGBTFlag"] == True:
                dcdcIgbtTemperatureMonitor = ValueRangeMonitor(munchInterop, dcdc.getIgbtTemperatures, shutdownAll, "DCDC IGBT Temperature")
                dcdcIgbtTemperatureMonitor.beginMonitoring(alignEncoder["safteyMonitors"]["DCDC"]["tempIGBTMin"], alignEncoder["safteyMonitors"]["DCDC"]["tempIGBTMax"])

            # Optional Gen5 safety monitors (Usage strongly advised)
            if alignEncoder["safteyMonitors"]["MCU"]["vdcFlag"] == True:
                gen5VoltageMonitor = ValueRangeMonitor(munchInterop, gen5.getDcLinkVoltage, shutdownAll, "Gen5 Voltage")
                gen5VoltageMonitor.beginMonitoring(alignEncoder["safteyMonitors"]["MCU"]["vdcMin"], alignEncoder["safteyMonitors"]["MCU"]["vdcMax"])
            
            if alignEncoder["safteyMonitors"]["MCU"]["tempMCUFlag"] == True:
                gen5InverterTemperatureMonitor = ValueRangeMonitor(munchInterop, gen5.getInverterTemperature, shutdownAll, "Gen5 Inverter Temperature")
                gen5InverterTemperatureMonitor.beginMonitoring(alignEncoder["safteyMonitors"]["MCU"]["tempMCUMin"], alignEncoder["safteyMonitors"]["MCU"]["tempMCUMax"])

            if alignEncoder["safteyMonitors"]["MCU"]["tempMotorFlag"] == True:
                gen5MotorTemperatureMonitor = ValueRangeMonitor(munchInterop, gen5.getMotorTemperature, shutdownAll, "Gen5 Motor Temperature")
                gen5MotorTemperatureMonitor.beginMonitoring(alignEncoder["safteyMonitors"]["MCU"]["tempMotorMin"], alignEncoder["safteyMonitors"]["MCU"]["tempMotorMax"])

            if alignEncoder["safteyMonitors"]["MCU"]["speedFlag"] == True:
                gen5SpeedMonitor = ValueRangeMonitor(munchInterop, gen5.getSpeed, shutdownAll, "Gen5 Speed")
                gen5SpeedMonitor.beginMonitoring(alignEncoder["safteyMonitors"]["MCU"]["speedMin"], alignEncoder["safteyMonitors"]["MCU"]["speedMax"])

            if alignEncoder["safteyMonitors"]["MCU"]["bridgeFlag"] == True:
                gen5BridgeMonitor = BridgeEnabledMonitor(munchInterop, gen5, shutdownAll, "Gen5 Bridge")
                gen5BridgeMonitor.beginMonitoring()

            # Optional Nidec Dyno safety monitors (Usage strongly advised)
            if alignEncoder["safteyMonitors"]["Dyno"]["tempIGBTFlag"] == True:
                dynoInverterTemperatureMonitor = ValueRangeMonitor(munchInterop, dyno.getInverterTemperature, shutdownAll, "Dyno Inverter Temperature")
                dynoInverterTemperatureMonitor.beginMonitoring(alignEncoder["safteyMonitors"]["Dyno"]["tempIGBTMin"], alignEncoder["safteyMonitors"]["Dyno"]["tempIGBTMax"])

            if alignEncoder["safteyMonitors"]["Dyno"]["AccFlag"] == True:
                dynoMotorProtectionAccumulatorMonitor = ValueRangeMonitor(munchInterop, dyno.getMotorProtectionAccumulator, shutdownAll, "Dyno Motor Protection Accumulator")
                dynoMotorProtectionAccumulatorMonitor.beginMonitoring(alignEncoder["safteyMonitors"]["Dyno"]["AccMin"], alignEncoder["safteyMonitors"]["Dyno"]["AccMax"])

            if alignEncoder["safteyMonitors"]["Dyno"]["speedFlag"] == True:
                dynoSpeedMonitor = ValueRangeMonitor(munchInterop, dyno.getSpeedFeedback, shutdownAll, "Dyno Speed")
                dynoSpeedMonitor.beginMonitoring(alignEncoder["safteyMonitors"]["Dyno"]["speedMin"], alignEncoder["safteyMonitors"]["Dyno"]["speedMax"])

            # DCDC CMDs
            print()
            print("~DCDC LIMITS~")
            print()
            
            print("DCDC Voltage Target: "       + str(alignEncoder["test"]["DCDC"]["voltageTarget"])  + "V")
            print("DCDC Current Limit [+]: "    + str(alignEncoder["test"]["DCDC"]["currentLimPos"])  + "A")
            print("DCDC Current Limit [-]: "    + str(alignEncoder["test"]["DCDC"]["currentLimNeg"])  + "A")

            dcdc.setOutputCurrentLimits(alignEncoder["test"]["DCDC"]["currentLimPos"], alignEncoder["test"]["DCDC"]["currentLimNeg"])
            dcdc.setOutputEnabled(True)
            dcdc.setOutputVoltage(alignEncoder["test"]["DCDC"]["voltageTarget"])

            timeout = time.time() + 10
            while(dcdc.getOutputVoltage() < alignEncoder["test"]["DCDC"]["voltageTarget"]*0.95) and (time.time() < timeout):
                print("Current DCDC Voltage: " + str(dcdc.getOutputVoltage()) + "V")
                time.sleep(0.1)

            # Setup MCU Limits
            print()
            print("~MCU LIMITS~")
            print()

            print("MCU Current Limit [+]: " + str(alignEncoder["test"]["MCU"]["currentLimPos"])+ "A")
            print("MCU Current Limit [-]: " + str(alignEncoder["test"]["MCU"]["currentLimNeg"])+ "A")
            mcu.setCurrentLimits(alignEncoder["test"]["MCU"]["currentLimPos"], alignEncoder["test"]["MCU"]["currentLimNeg"])

            print("MCU Mod Index Limit: ("+str(alignEncoder["test"]["udq"]["modIndexID"])+"): " + str(alignEncoder["test"]["udq"]["targetModIndex"]) + "%")
            mcu.writeBaseCommsProperty(propertyIDs["modIndex"], alignEncoder["setup"]["targetModIndex"], True)

            print("MCU Forward Speed Limit:" + str(alignEncoder["test"]["udq"]["mcuForwardSpeedLimit"]) + "rpm")
            print("MCU Reverse Speed Limit:" + str(alignEncoder["test"]["udq"]["mcuReverseSpeedLimit"]) + "rpm")
            mcu.setSpeedLimits(alignEncoder["test"]["udq"]["mcuForwardSpeedLimit"], alignEncoder["test"]["udq"]["mcuReverseSpeedLimit"])
            
            #Enable Bridge
            if mcu.setBridgeEnabled(True):
                print()
                print("~~~ START OF ENCODER ALIGNMENT ~~~")
                print()

                dyno.setDriveEnabled(True)
                dyno.setInverterEnabled(True)   
                dyno.setForwardReverse(True) 

                print("MCU, Dyno Drive and Inverter enabled.")
                time.sleep(1)
                print("Rotating Dyno..")

                dynoSpeedStep = 0.0
                while(mcu.readBaseCommsProperty(propertyIDs["modIndex"], True) < alignEncoder["test"]["udq"]["targetModIndex"]) and ( (dyno.getSpeedFeedback() / alignEncoder["setup"]["dynoGear"]) < (alignEncoder["test"]["udq"]["mcuForwardSpeedLimit"] or alignEncoder["test"]["udq"]["mcuReverseSpeedLimit"])):
                    
                    print("Current Dyno Speed: " + str(dyno.getSpeedFeedback()/alignEncoder["setup"]["dynoGear"]) + "rpm")
                    print("Current Mod Index: " + str(mcu.readBaseCommsProperty(propertyIDs["modIndex"], True)))
                    time.sleep(0.1)
                    dyno.setPresetReference1(dynoSpeedStep/alignEncoder["setup"]["dynoGear"])
                    dynoSpeedStep = dynoSpeedStep + 100
                    time.sleep(0.1)
                
                print("Mod Index Target reached, disabling bridge..")
                mcu.setBridgeEnabled(False)

            print("~~~ DATA CAPTURE: START ~~~")

            print("Coasting down with bridge disabled..")
            print("Logging Ud, Uq, Motor Under Test Speed and Modulation Index.")
            while( abs(dyno.getSpeedFeedback() / alignEncoder["setup"]["dynoGear"]) > 10.0 ):
                resultUd.append(mcu.readBaseCommsProperty(propertyIDs["ud"], True))
                resultUq.append(mcu.readBaseCommsProperty(propertyIDs["uq"], True))
                resultMUTSpeed.append(mcu.readBaseCommsProperty(propertyIDs["motorSpeed"], True))
                resultModIndex.append(mcu.readBaseCommsProperty(propertyIDs["modIndex"], True))
                time.sleep(0.1)

            print("Dyno Speed has reached less than 10rpm, stopped logging.")

            print("~~~ DATA CAPTURE: END ~~~")

        
                    
        except Exception:
            raise

    angleTable              = pd.DataFrame(data = [{"oldEncoderAngleDegrees":oldEncoderAngleDegrees,"newEncoderAngleDegrees":newEncoderAngleDegrees,"oldEncoderAngleRadians":oldEncoderAngleRadians,"newEncoderAngleRadians":newEncoderAngleRadians}])
    alignTable              = pd.DataFrame(data = [{"Speed":resultMUTSpeed,"Ud":resultUd,"Uq":resultUq, "ModulationIndex":resultModIndex}])

    return angleTable, alignTable

def alignEncoderIdTorque():
    print("Placeholder")
    return

def plotEncoderSns(alignTable):
    pairPlot = sns.pairplot(alignTable)
    return pairPlot

def calculateEncoderAngleUdq(alignTable):
    model = LinearRegression()

def plotEncoderPlotly(alignTable):

# udq plot
    udqPlot = go.Figure()

    udqPlot.add_trace	(go.Scattergl (  
                           x       		= alignTable.Ud,
                           y       		= alignTable.Uq,
                           name 	    = "Ud : Uq",
                           mode         = 'markers',
                                   )
                   )

    udqSpeedPlot = go.Figure()

    udqSpeedPlot.add_trace	(go.Scattergl (  
                           x       		= alignTable.Speed,
                           y       		= alignTable.Ud,
                           yaxis        = "y1"
                                   )
                   )

    udqSpeedPlot.add_trace	(go.Scattergl (  
                           x       		= alignTable.Speed,
                           y       		= alignTable.Uq,
                           yaxis        = "y2"
                                   )
                   )

# udq vs speed plot with y1 and y2 axis.
    udqSpeedPlot.update_layout  (
                                    yaxis=dict(
                                        title="Ud [V rms]",
                                    ),
                                    yaxis1=dict(
                                        title="Uq [V rms]",
                                        anchor="x",
                                        overlaying="y",
                                        side="right"
                                    ),
                                    title       = "Speed (Ud:Uq)",
                                    xaxis_title = "Speed (rpm)",
                                    hovermode="x unified",
                                    legend=dict(
                                    orientation="h",
                                    yanchor="bottom",
                                    y=1.0,
                                    xanchor="right",
                                    x=1)
                                )

    return udqPlot, udqSpeedPlot

def saveAlignEncoder(encoderAngleProperty, encoderAngleValue):
    new = 0
    old = 0

    if new != old:
        saved = True
    else:
        saved = False
    
    return saved