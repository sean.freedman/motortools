import streamlit as st

textComissionRPS = "If commissioning the RPS, it should be wired to the inverter with appropriate shielding and grounding in place. See relevant motor and inverter wiring diagrams. Also - if available - ensure sensor latency tables are zeroed i.e the angle values are all set to 0 degrees."
textEncoderMethodUdq = "This method will rotate the motor to the until the modulation index reaches the requested value, disables the inverter bridge and records the Udq voltages until the speed coasts to zero. Following this the encoder angle will be adjusted until the normal distribution of the voltages are aligned with the y-axis"
textEncoderMethodIdTorque = ""
textSetup = "Use the following fields to populate details about the test equipment, some fields will try to auto-fill. However if these details are incorrect, you can overwrite them."
urlMCUPinout = '[MCU Pinouts](https://turntidetech.sharepoint.com/)'
urlEncoderPinout = '[Encoder Pinouts](https://turntidetech.sharepoint.com/)'
textEncoderMethodIdTorque = ""
textEncoderLatency = "Placeholder for Encoder Latency Text Description"
textCurrentLatency = "Placeholder for Current Sensor Latency Text Description"