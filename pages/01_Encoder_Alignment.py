import streamlit as st
import pandas as pd
from src.encoder import getCurrentEncoderAlignment, alignEncoderUdq, alignEncoderIdTorque, plotEncoderSns, saveAlignEncoder
from src.documentation import textComissionRPS, textEncoderMethodUdq, textEncoderMethodIdTorque, urlEncoderPinout, urlMCUPinout

#Intialisation
alignEncoder                = None

# START PAGE

st.header("Encoder Alignment")

st.info(textComissionRPS)

st.markdown(urlMCUPinout + " | " + urlEncoderPinout , unsafe_allow_html=True)

st.write("---")

st.subheader("Saftey Monitors ⚠️")

with st.expander("MCU"):
    smcu1, smcu2, smcu3 = st.columns(3)
    
    st.session_state["safteyMonitorMCU"]["vdcFlag"]         = smcu1.radio("DC Link Voltage", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorMCU"]["vdcMin"]          = smcu2.number_input("Minimum VDC", min_value = -1000.0, max_value = 0.0, value = st.session_state["safteyMonitorMCU"]["vdcMin"], step = 1.0)
    st.session_state["safteyMonitorMCU"]["vdcMax"]          = smcu3.number_input("Maximum VDC", min_value = -0.0, max_value = 1000.0, value = st.session_state["safteyMonitorMCU"]["vdcMax"], step = 1.0)

    st.session_state["safteyMonitorMCU"]["idcFlag"]         = smcu1.radio("DC Link Current", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorMCU"]["idcMin"]          = smcu2.number_input("Minimum IDC", min_value = -1000.0, max_value = 0.0, value = st.session_state["safteyMonitorMCU"]["idcMin"], step = 1.0)
    st.session_state["safteyMonitorMCU"]["idcMax"]          = smcu3.number_input("Maximum IDC", min_value = 0.0, max_value = 1000.0, value = st.session_state["safteyMonitorMCU"]["idcMax"], step = 1.0)

    st.session_state["safteyMonitorMCU"]["speedFlag"]       = smcu1.radio("Motor Speed", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorMCU"]["speedMin"]        = smcu2.number_input("Minimum MUT Speed", min_value = -20000.0, max_value = 0.0, value = st.session_state["safteyMonitorMCU"]["speedMin"], step = 1.0)
    st.session_state["safteyMonitorMCU"]["speedMax"]        = smcu3.number_input("Maximum MUT Speed", min_value = 0.0, max_value = 20000.0, value = st.session_state["safteyMonitorMCU"]["speedMax"], step = 1.0)

    st.session_state["safteyMonitorMCU"]["torqueFlag"]      = smcu1.radio("Motor Torque", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorMCU"]["torqueMin"]       = smcu2.number_input("Minimum MUT Torque", min_value = -1000.0, max_value = 0.0, value = st.session_state["safteyMonitorMCU"]["torqueMin"], step = 1.0)
    st.session_state["safteyMonitorMCU"]["torqueMax"]       = smcu3.number_input("Maximum MUT Torque", min_value = -0.0, max_value = 1000.0, value = st.session_state["safteyMonitorMCU"]["torqueMax"], step = 1.0)

    st.session_state["safteyMonitorMCU"]["tempMotorFlag"]   = smcu1.radio("Motor Temperature", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorMCU"]["tempMotorMin"]    = smcu2.number_input("Minimum MUT Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorMCU"]["tempMotorMin"], step = 1.0)
    st.session_state["safteyMonitorMCU"]["tempMotorMax"]    = smcu3.number_input("Maximum MUT Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorMCU"]["tempMotorMax"], step = 1.0)

    st.session_state["safteyMonitorMCU"]["tempMCUFlag"]     = smcu1.radio("MCU Temperature", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorMCU"]["tempMCUMin"]      = smcu2.number_input("Minimum MCU Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorMCU"]["tempMCUMin"], step = 1.0)
    st.session_state["safteyMonitorMCU"]["tempMCUMax"]      = smcu3.number_input("Maximum MCU Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorMCU"]["tempMCUMax"], step = 1.0)

    st.session_state["safteyMonitorMCU"]["tempIGBTFlag"]    = smcu1.radio("MCU IGBT", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorMCU"]["tempIGBTMin"]     = smcu2.number_input("Minimum MCU IGBT Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorMCU"]["tempIGBTMin"], step = 1.0)
    st.session_state["safteyMonitorMCU"]["tempIGBTMax"]     = smcu3.number_input("Maximum MCU IGBT Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorMCU"]["tempIGBTMax"], step = 1.0)

    st.session_state["safteyMonitorMCU"]["bridgeFlag"]      = smcu1.radio("Bridge State", [True, False], help="Will shutdown testing it either of the thresholds are reached.")

with st.expander("DCDC"):

    sdcdc1, sdcdc2, sdcdc3 = st.columns(3)

    st.session_state["safteyMonitorDCDC"]["vdcFlag"]        = sdcdc1.radio("DCDC Voltage", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorDCDC"]["vdcMin"]         = sdcdc2.number_input("Minimum DCDC V", min_value = -1000.0, max_value = 0.0, value = st.session_state["safteyMonitorDCDC"]["vdcMin"], step = 1.0)
    st.session_state["safteyMonitorDCDC"]["vdcMax"]         = sdcdc3.number_input("Maximum DCDC V", min_value = -0.0, max_value = 1000.0, value = st.session_state["safteyMonitorDCDC"]["vdcMax"], step = 1.0)

    st.session_state["safteyMonitorDCDC"]["tempIGBTFlag"]   = sdcdc1.radio("DCDC IGBT Temperature", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorDCDC"]["tempIGBTMin"]    = sdcdc2.number_input("Minimum DCDC I", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorDCDC"]["tempIGBTMin"], step = 1.0)
    st.session_state["safteyMonitorDCDC"]["tempIGBTMax"]    = sdcdc3.number_input("Maximum DCDC I", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorDCDC"]["tempIGBTMax"], step = 1.0)

with st.expander("Dyno"):

    sdyno1, sdyno2, sdyno3 = st.columns(3)
    st.session_state["safteyMonitorDyno"]["speedFlag"]      = sdyno1.radio("Dyno Speed", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorDyno"]["speedMin"]       = sdyno2.number_input("Minimum Syno Speed", min_value = -20000.0, max_value = 0.0, value = st.session_state["safteyMonitorDyno"]["speedMin"], step = 1.0)
    st.session_state["safteyMonitorDyno"]["speedMax"]       = sdyno3.number_input("Maximum Syno Speed", min_value = 0.0, max_value = 20000.0, value = st.session_state["safteyMonitorDyno"]["speedMax"], step = 1.0)

    st.session_state["safteyMonitorDyno"]["tempIGBTFlag"]   = sdyno1.radio("Dyno IGBT Temperature", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorDyno"]["tempIGBTMin"]    = sdyno2.number_input("Minimum Dyno IGBT Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorDyno"]["tempIGBTMin"], step = 1.0)
    st.session_state["safteyMonitorDyno"]["tempIGBTMax"]    = sdyno3.number_input("Maximum Dyno IGBT Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorDyno"]["tempIGBTMax"], step = 1.0)

    st.session_state["safteyMonitorDyno"]["AccFlag"]        = sdyno1.radio("Dyno Accumlation Protection", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorDyno"]["AccMin"]         = sdyno2.number_input("Minimum Dyno Accumlation", min_value = 0.0, max_value = 200.0, value = float(st.session_state["safteyMonitorDyno"]["AccMin"]), step = 1.0)
    st.session_state["safteyMonitorDyno"]["AccMax"]         = sdyno3.number_input("Maximum Dyno Accumlation", min_value = 0.0, max_value = 200.0, value = float(st.session_state["safteyMonitorDyno"]["AccMax"]), step = 1.0)

st.write("---")

st.subheader("Aligment Configuration")


st.markdown("**DCDC**")
dc1, dc2, dc3 = st.columns(3)

st.session_state["alignEncoder"]["test"]["DCDC"]["voltageTarget"]   = dc1.number_input("DCDC Target Voltage [V]", min_value = 0.0,      max_value = 400.0,    value = st.session_state["alignEncoder"]["test"]["DCDC"]["voltageTarget"], step = 1.0)
st.session_state["alignEncoder"]["test"]["DCDC"]["currentLimPos"]   = dc2.number_input("DCDC Current Limit [+]",  min_value = 0.0,      max_value = 500.0,    value = st.session_state["alignEncoder"]["test"]["DCDC"]["currentLimPos"], step = 1.0)
st.session_state["alignEncoder"]["test"]["DCDC"]["currentLimNeg"]   = dc3.number_input("DCDC Current Limit [-]",  min_value = -500.0,   max_value = 0.0,      value = st.session_state["alignEncoder"]["test"]["DCDC"]["currentLimNeg"], step = 1.0)

st.markdown("**MCU**")
mcu1, mcu2 = st.columns(2)

st.session_state["alignEncoder"]["test"]["MCU"]["currentLimPos"]    = mcu1.number_input("MCU Current Limit [+]",    min_value = 0.0,      max_value = 400.0,    value = st.session_state["alignEncoder"]["test"]["MCU"]["currentLimPos"], step = 1.0)
st.session_state["alignEncoder"]["test"]["MCU"]["currentLimNeg"]    = mcu2.number_input("MCU Current Limit [-]",    min_value = -800.0,   max_value = 0.0,      value = st.session_state["alignEncoder"]["test"]["MCU"]["currentLimNeg"], step = 1.0)

st.markdown("**Method**")

m1, m2 = st.columns(2)

st.session_state["alignEncoder"]["test"]["method"]                  = m1.selectbox("Encoder Alignment Method", ["Udq","Id Torque"])
st.session_state["alignEncoder"]["test"]["encoderAngleOffsetID"]    = m2.text_input("Encoder Offset Property ID", value = st.session_state["alignEncoder"]["test"]["encoderAngleOffsetID"] )

# Get required Inputs for aligment scripts

st.markdown("**Encoder Method Configuration**")
c11,c22,c33= st.columns(3)
st.write("---")

alignEncoder = {
        
        "setup"                             : {
            "ixxatMCU"                      : st.session_state["setup"]["ixxatMCU"],
            "ixxatDCDC"                     : st.session_state["setup"]["ixxatDCDC"],
            "dcdcConfig"                    : st.session_state["setup"]["dcdcConfig"],
            "project"                       : st.session_state["setup"]["project"],
            "dynoLocation"                  : st.session_state["setup"]["dynoLocation"],
            "dynoIP"                        : st.session_state["setup"]["dynoIP"],
        },      

        "safteyMonitors"                    : {
            "MCU"                           : {
                "vdcFlag"                   : st.session_state["safteyMonitorMCU"]["vdcFlag"],
                "vdcMin"                    : st.session_state["safteyMonitorMCU"]["vdcMin"],
                "vdcMax"                    : st.session_state["safteyMonitorMCU"]["vdcMax"],
                "tempMCUFlag"               : st.session_state["safteyMonitorMCU"]["vdcFlag"],
                "tempMCUMin"                : st.session_state["safteyMonitorMCU"]["tempMCUMin"],
                "tempMCUMax"                : st.session_state["safteyMonitorMCU"]["tempMCUMax"],
                "tempMotorFlag"             : st.session_state["safteyMonitorMCU"]["vdcFlag"],
                "tempMotorMax"              : st.session_state["safteyMonitorMCU"]["tempMotorMax"],
                "tempMotorMin"              : st.session_state["safteyMonitorMCU"]["tempMotorMin"],
                "speedFlag"                 : st.session_state["safteyMonitorMCU"]["vdcFlag"],
                "speedMax"                  : st.session_state["safteyMonitorMCU"]["speedMax"],
                "speedMin"                  : st.session_state["safteyMonitorMCU"]["speedMin"],
                "bridgeFlag"                : st.session_state["safteyMonitorMCU"]["bridgeFlag"]
            },      

            "DCDC"                          : {
                "vdcFlag"                   : st.session_state["safteyMonitorDCDC"]["vdcFlag"],
                "vdcMin"                    : st.session_state["safteyMonitorDCDC"]["vdcMin"],
                "vdcMax"                    : st.session_state["safteyMonitorDCDC"]["vdcMax"],
                "tempIGBTFlag"              : st.session_state["safteyMonitorDCDC"]["tempIGBTFlag"],
                "tempIGBTMin"               : st.session_state["safteyMonitorDCDC"]["tempIGBTMin"],
                "tempIGBTMax"               : st.session_state["safteyMonitorDCDC"]["tempIGBTMax"],
            },      

            "Dyno"                          : {
                "AccFlag"                   : st.session_state["safteyMonitorDyno"]["AccFlag"],
                "AccMin"                    : st.session_state["safteyMonitorDyno"]["AccMin"],
                "AccMax"                    : st.session_state["safteyMonitorDyno"]["AccMax"],
                "tempIGBTFlag"              : st.session_state["safteyMonitorDyno"]["tempIGBTFlag"],
                "tempIGBTMin"               : st.session_state["safteyMonitorDyno"]["tempIGBTMin"],
                "tempIGBTMax"               : st.session_state["safteyMonitorDyno"]["tempIGBTMax"],
                "speedFlag"                 : st.session_state["safteyMonitorDyno"]["speedFlag"],
                "speedMax"                  : st.session_state["safteyMonitorDyno"]["speedMax"],
                "speedMin"                  : st.session_state["safteyMonitorDyno"]["speedMin"],
            },      
        },

        "test"                              : {
            "DCDC"                          : {
                "voltageTarget"             : st.session_state["alignEncoder"]["test"]["DCDC"]["voltageTarget"],
                "currentLimPos"             : st.session_state["alignEncoder"]["test"]["DCDC"]["currentLimPos"],
                "currentLimNeg"             : st.session_state["alignEncoder"]["test"]["DCDC"]["currentLimNeg"],
            },
            "MCU"                           : {
                "currentLimPos"             : st.session_state["alignEncoder"]["test"]["DCDC"]["currentLimPos"],
                "currentLimNeg"             : st.session_state["alignEncoder"]["test"]["DCDC"]["currentLimNeg"],
            },
            "encoderAngleOffsetID"          : st.session_state["alignEncoder"]["test"]["encoderAngleOffsetID"],
            "method"                        : st.session_state["alignEncoder"]["test"]["method"],
            "udq"                           : {
                "targetModIndex"            : 0,
                "mcuForwardSpeedLimit"      : 0,
                "mcuReverseSpeedLimit"      : 0,          
            },
            "idTorque"                      : {
                "targetIdCurrent"           : 0,         
            },
        }     
    }

if alignEncoder["test"]["method"] == "Udq":


    st.session_state["alignEncoder"]["test"]["udq"]["targetModIndex"]       = c11.number_input("Target Mod Index [%]",  min_value = 0, max_value = 100, value = st.session_state["alignEncoder"]["test"]["udq"]["targetModIndex"], step = 1)
    st.session_state["alignEncoder"]["test"]["udq"]["mcuForwardSpeedLimit"] = c22.number_input("MCU Forward Speed Limit [+]", min_value = 0, max_value = 20000,  value = int(st.session_state["alignEncoder"]["test"]["udq"]["mcuForwardSpeedLimit"]), step = 1)
    st.session_state["alignEncoder"]["test"]["udq"]["mcuReverseSpeedLimit"] = c33.number_input("MCU Reverse Speed Limit [-]", min_value = -20000, max_value = 0, value = int(st.session_state["alignEncoder"]["test"]["udq"]["mcuReverseSpeedLimit"]), step = 1)
    
    alignEncoder["test"]["udq"]["targetModIndex"]       = st.session_state["alignEncoder"]["test"]["udq"]["targetModIndex"]      
    alignEncoder["test"]["udq"]["mcuForwardSpeedLimit"] = st.session_state["alignEncoder"]["test"]["udq"]["mcuForwardSpeedLimit"]
    alignEncoder["test"]["udq"]["mcuReverseSpeedLimit"] = st.session_state["alignEncoder"]["test"]["udq"]["mcuReverseSpeedLimit"]
    
    st.info("Ensure Idq tables are present in the software (default state)")
    st.write(textEncoderMethodUdq)

    with st.expander("Information"):
        st.write("Markdown of Obsidian File?")

    if st.button("Read MCU Encoder Angle"):
        try:
            angleRadians, angleDegrees = getCurrentEncoderAlignment(alignEncoder)
            angleTable = pd.DataFrame(data = [{"Radians":angleRadians, "Degrees":angleDegrees}], index=["Angle"])
            st.write(angleTable)
        except Exception:
            raise
            st.warning("Failed to get Encoder Offset Angle, Make sure MCU is connected.")

    if st.button("Align Encoder"):
        alignEncoder = "Udq"


if alignEncoder["test"]["method"] == "Id Torque":
    st.info("Ensure Test Mode is enabled on the software, otherwise Id Current cannot be applied.")
    st.write(textEncoderMethodIdTorque)

    with st.expander("Information"):
        st.write("Markdown of Obsidian File?")

    st.session_state["alignEncoder"]["test"]["idTorque"]["targetIdCurrent"] = c11.number_input("Target Id Current",  min_value = -500.0, max_value = 0.0, value = st.session_state["alignEncoder"]["test"]["idTorque"]["targetIdCurrent"], step = 1.0)
    
    alignEncoder["test"]["idTorque"]["targetIdCurrent"] = st.session_state["alignEncoder"]["test"]["idTorque"]["targetIdCurrent"]

    if st.button("Align Encoder"):
        alignEncoder = "IdTorque"

if alignEncoder == "Udq":
    # alignEncoderUdq(alignEncoderInputs, safteyMonitorMCU, safteyMonitorDCDC, safteyMonitorDyno)
    plotEncoder()

    if st.button("Save Values into Controller?"):
        saveAlignEncoder()

elif alignEncoder == "IdTorque":
    #alignEncoderIdTorque()
    plotEncoder()

    if st.button("Save Values into Controller?"):
        saveAlignEncoder()

else:
    pass
