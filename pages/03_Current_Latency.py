import streamlit as st
from src.trimtable import getCurrentTrimTable, plotCurrentTrimTable, getCurrentTrimTableInputs, calculateCurrentTrimTable, saveCurrentTrimTable, checkCurrentTrimTableSave
from src.text import textCurrentLatency

st.title("Current Latency Table")

encoderPropertyIdDefault    = "0x20180015"
currentTrimTableIdDefault   = "0x20180015"

st.write(textCurrentLatency)

st.write("---")

st.subheader("Saftey Monitors ⚠️")

with st.expander("MCU"):
    smcu1, smcu2, smcu3 = st.columns(3)
    
    st.session_state["safteyMonitorMCU"]["vdcFlag"]         = smcu1.radio("DC Link Voltage", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorMCU"]["vdcMin"]          = smcu2.number_input("Minimum VDC", min_value = -1000.0, max_value = 0.0, value = st.session_state["safteyMonitorMCU"]["vdcMin"], step = 1.0)
    st.session_state["safteyMonitorMCU"]["vdcMax"]          = smcu3.number_input("Maximum VDC", min_value = -0.0, max_value = 1000.0, value = st.session_state["safteyMonitorMCU"]["vdcMax"], step = 1.0)

    st.session_state["safteyMonitorMCU"]["idcFlag"]         = smcu1.radio("DC Link Current", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorMCU"]["idcMin"]          = smcu2.number_input("Minimum IDC", min_value = -1000.0, max_value = 0.0, value = st.session_state["safteyMonitorMCU"]["idcMin"], step = 1.0)
    st.session_state["safteyMonitorMCU"]["idcMax"]          = smcu3.number_input("Maximum IDC", min_value = 0.0, max_value = 1000.0, value = st.session_state["safteyMonitorMCU"]["idcMax"], step = 1.0)

    st.session_state["safteyMonitorMCU"]["speedFlag"]       = smcu1.radio("Motor Speed", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorMCU"]["speedMin"]        = smcu2.number_input("Minimum MUT Speed", min_value = -20000.0, max_value = 0.0, value = st.session_state["safteyMonitorMCU"]["speedMin"], step = 1.0)
    st.session_state["safteyMonitorMCU"]["speedMax"]        = smcu3.number_input("Maximum MUT Speed", min_value = 0.0, max_value = 20000.0, value = st.session_state["safteyMonitorMCU"]["speedMax"], step = 1.0)

    st.session_state["safteyMonitorMCU"]["torqueFlag"]      = smcu1.radio("Motor Torque", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorMCU"]["torqueMin"]       = smcu2.number_input("Minimum MUT Torque", min_value = -1000.0, max_value = 0.0, value = st.session_state["safteyMonitorMCU"]["torqueMin"], step = 1.0)
    st.session_state["safteyMonitorMCU"]["torqueMax"]       = smcu3.number_input("Maximum MUT Torque", min_value = -0.0, max_value = 1000.0, value = st.session_state["safteyMonitorMCU"]["torqueMax"], step = 1.0)

    st.session_state["safteyMonitorMCU"]["tempMotorFlag"]   = smcu1.radio("Motor Temperature", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorMCU"]["tempMotorMin"]    = smcu2.number_input("Minimum MUT Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorMCU"]["tempMotorMin"], step = 1.0)
    st.session_state["safteyMonitorMCU"]["tempMotorMax"]    = smcu3.number_input("Maximum MUT Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorMCU"]["tempMotorMax"], step = 1.0)

    st.session_state["safteyMonitorMCU"]["tempMCUFlag"]     = smcu1.radio("MCU Temperature", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorMCU"]["tempMCUMin"]      = smcu2.number_input("Minimum MCU Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorMCU"]["tempMCUMin"], step = 1.0)
    st.session_state["safteyMonitorMCU"]["tempMCUMax"]      = smcu3.number_input("Maximum MCU Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorMCU"]["tempMCUMax"], step = 1.0)

    st.session_state["safteyMonitorMCU"]["tempIGBTFlag"]    = smcu1.radio("MCU IGBT", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorMCU"]["tempIGBTMin"]     = smcu2.number_input("Minimum MCU IGBT Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorMCU"]["tempIGBTMin"], step = 1.0)
    st.session_state["safteyMonitorMCU"]["tempIGBTMax"]     = smcu3.number_input("Maximum MCU IGBT Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorMCU"]["tempIGBTMax"], step = 1.0)

    st.session_state["safteyMonitorMCU"]["bridgeFlag"]      = smcu1.radio("Bridge State", [True, False], help="Will shutdown testing it either of the thresholds are reached.")

with st.expander("DCDC"):

    sdcdc1, sdcdc2, sdcdc3 = st.columns(3)

    st.session_state["safteyMonitorDCDC"]["vdcFlag"]        = sdcdc1.radio("DCDC Voltage", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorDCDC"]["vdcMin"]         = sdcdc2.number_input("Minimum DCDC V", min_value = -1000.0, max_value = 0.0, value = st.session_state["safteyMonitorDCDC"]["vdcMin"], step = 1.0)
    st.session_state["safteyMonitorDCDC"]["vdcMax"]         = sdcdc3.number_input("Maximum DCDC V", min_value = -0.0, max_value = 1000.0, value = st.session_state["safteyMonitorDCDC"]["vdcMax"], step = 1.0)

    st.session_state["safteyMonitorDCDC"]["tempIGBTFlag"]   = sdcdc1.radio("DCDC IGBT Temperature", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorDCDC"]["tempIGBTMin"]    = sdcdc2.number_input("Minimum DCDC I", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorDCDC"]["tempIGBTMin"], step = 1.0)
    st.session_state["safteyMonitorDCDC"]["tempIGBTMax"]    = sdcdc3.number_input("Maximum DCDC I", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorDCDC"]["tempIGBTMax"], step = 1.0)

with st.expander("Dyno"):

    sdyno1, sdyno2, sdyno3 = st.columns(3)
    st.session_state["safteyMonitorDyno"]["speedFlag"]      = sdyno1.radio("Dyno Speed", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorDyno"]["speedMin"]       = sdyno2.number_input("Minimum Syno Speed", min_value = -20000.0, max_value = 0.0, value = st.session_state["safteyMonitorDyno"]["speedMin"], step = 1.0)
    st.session_state["safteyMonitorDyno"]["speedMax"]       = sdyno3.number_input("Maximum Syno Speed", min_value = 0.0, max_value = 20000.0, value = st.session_state["safteyMonitorDyno"]["speedMax"], step = 1.0)

    st.session_state["safteyMonitorDyno"]["tempIGBTFlag"]   = sdyno1.radio("Dyno IGBT Temperature", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorDyno"]["tempIGBTMin"]    = sdyno2.number_input("Minimum Dyno IGBT Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorDyno"]["tempIGBTMin"], step = 1.0)
    st.session_state["safteyMonitorDyno"]["tempIGBTMax"]    = sdyno3.number_input("Maximum Dyno IGBT Temp", min_value = -200.0, max_value = 200.0, value = st.session_state["safteyMonitorDyno"]["tempIGBTMax"], step = 1.0)

    st.session_state["safteyMonitorDyno"]["AccFlag"]        = sdyno1.radio("Dyno Accumlation Protection", [True, False], help="Will shutdown testing it either of the thresholds are reached.")
    st.session_state["safteyMonitorDyno"]["AccMin"]         = sdyno2.number_input("Minimum Dyno Accumlation", min_value = 0.0, max_value = 200.0, value = st.session_state["safteyMonitorDyno"]["AccMin"], step = 1.0)
    st.session_state["safteyMonitorDyno"]["AccMax"]         = sdyno3.number_input("Maximum Dyno Accumlation", min_value = 0.0, max_value = 200.0, value = st.session_state["safteyMonitorDyno"]["AccMax"], step = 1.0)

st.write("---")


getCurrentTrimTable()
plotCurrentTrimTable()
getCurrentTrimTableInputs()
calculateCurrentTrimTable()
plotCurrentTrimTable()

if st.button("Save Values into Controller?"):
    saveCurrentTrimTable()

    if checkCurrentTrimTableSave() == True:
        st.success("Current Trim Table Saved into Controller")
    else:
        st.error("Saving Current Trim Table Failed")
