
from pyrsistent import b
import streamlit as st
import os
from src.documentation import textSetup
import time

@st.cache(allow_output_mutation=True)
def intialise():
    projects = [
        "Derwent",
        "Bowfell",
        "Oxford",
        "Other"
    ]

    dynoLocations = [
        "DX: 340kW",
        "DX: 160kW",
        "DX: 100kW"
    ]
    
    initialSetup = {
        "loggingPath"       : os.getcwd()+"\Log",
        "project"           : '',
        "dynoGear"          : 0.0,
        "dynoLocation"      : dynoLocations[0],
        "dynoLocationIndex" : 0,
        "dynoIP"            : "",
        "dynoID"            : 0.0,
        "dynoPort"          : 0.0,
        "ixxatMCU"          : "",
        "ixxatDCDC"         : "",
        "dcdcConfig"        : "",
        "setupDefaults"     : True
        
        }

    initialSafteyMonitorDyno = {
            "speedMin"      : -100.0,
            "speedMax"      : 500.0,
            "vdcFlag"       : True,
            "AccMin"        : 0.0,
            "AccMax"        : 90.0,
            "AccFlag"       : True,
            "tempIGBTMin"   : 0.0,
            "tempIGBTMax"   : 120.0,
            "tempIGBTFlag"  : True,
        }

    intialSafteyMonitorDCDC = {
            "vdcMin"        : -100.0,
            "vdcMax"        : 500.0,
            "vdcFlag"       : True,
            "tempIGBTMin"   : 0.0,
            "tempIGBTMax"   : 120.0,
            "tempIGBTFlag"  : True,
        }
    
    initialSafteyMonitorMCU = {
        "vdcMin"            : -10.0,
        "vdcMax"            : 500.0,
        "vdcFlag"           : True,
        "idcMin"            : -500.0,
        "idcMax"            : 500.0,
        "idcFlag"           : True,
        "speedMin"          : -5000.0,
        "speedMax"          : 5000.0,
        "speedFlag"         : True,
        "torqueMin"         : -1000.0,
        "torqueMax"         : 1000.0,
        "torqueFlag"        : True,
        "tempMotorMin"      : 0.0,
        "tempMotorMax"      : 120.0,
        "tempMotorFlag"     : True,
        "tempMCUMin"        : 0.0,
        "tempMCUMax"        : 120.0,
        "tempMCUFlag"       : True,
        "tempIGBTMin"       : 0.0,
        "tempIGBTMax"       : 120.0,
        "tempIGBTFlag"      : True,
    }

    initialAlignEncoder = {
        "test"                              : {
            "DCDC"                          : {
                "voltageTarget"             : 0.0,
                "currentLimPos"             : 500.0,
                "currentLimNeg"             : -500.0,
            },
            "MCU"                           : {
                "currentLimPos"             : 400.0,
                "currentLimNeg"             : -400.0,
            },
            "encoderAngleOffsetID"          : 0.0,
            "method"                        : "",
            "udq"                           : {
                "targetModIndex"            : 0.0,
                "mcuForwardSpeedLimit"      : 500.0,
                "mcuReverseSpeedLimit"      : -500.0,          
            },

            "idTorque"                      : {
                "targetIdCurrent"           : 0.0,         
                },
        }
    }
    return projects, dynoLocations, initialSetup, initialSafteyMonitorDyno, intialSafteyMonitorDCDC, initialSafteyMonitorMCU, initialAlignEncoder


def dictToSessionState(sub_dictionary_name, input_dictionary):
        if sub_dictionary_name not in st.session_state:
            st.session_state[str(sub_dictionary_name)] = dict()

        for i in range(0, len(input_dictionary)):
            if list(input_dictionary.keys())[i] not in st.session_state[str(sub_dictionary_name)]:
                st.session_state[str(sub_dictionary_name)][ str(list(input_dictionary.keys())[i]) ] = list(input_dictionary.values())[i]

def app():
    # Intiailsation

    projects, dynoLocations, initialSetup, initialSafteyMonitorDyno, intialSafteyMonitorDCDC, initialSafteyMonitorMCU, initialAlignEncoder = intialise() 

    dictToSessionState("setup",initialSetup)
    dictToSessionState("safteyMonitorDyno",initialSafteyMonitorDyno)
    dictToSessionState("safteyMonitorDCDC",intialSafteyMonitorDCDC)
    dictToSessionState("safteyMonitorMCU", initialSafteyMonitorMCU)
    dictToSessionState("alignEncoder", initialAlignEncoder)

    # START PAGE
    st.header("Setup")
    st.markdown(textSetup)

    # LOGGING
    st.subheader("Logging")
    st.session_state["setup"]["loggingPath"] = st.text_input("Log Directory", value = st.session_state["setup"]["loggingPath"])

    clmInverter,clmMotor = st.columns(2)

    # INVERTER
    clmInverter.subheader("Inverter")

    def updateProjectDefaults():
        
        if st.session_state["setup"]["project"] == projects[0]:
            st.session_state["safteyMonitorMCU"]["vdcMin"]          = -10.0
            st.session_state["safteyMonitorMCU"]["vdcMax"]          = 500.0
            st.session_state["safteyMonitorMCU"]["vdcFlag"]         = True
            st.session_state["safteyMonitorMCU"]["idcMin"]          = -500.0
            st.session_state["safteyMonitorMCU"]["idcMax"]          = 500.0
            st.session_state["safteyMonitorMCU"]["idcFlag"]         = True
            st.session_state["safteyMonitorMCU"]["speedMin"]        = -5000.0
            st.session_state["safteyMonitorMCU"]["speedMax"]        = 5000.0
            st.session_state["safteyMonitorMCU"]["speedFlag"]       = True
            st.session_state["safteyMonitorMCU"]["torqueMin"]       = -1000.0
            st.session_state["safteyMonitorMCU"]["torqueMax"]       = 1000.0
            st.session_state["safteyMonitorMCU"]["torqueFlag"]      = True
            st.session_state["safteyMonitorMCU"]["tempMotorMin"]    = 0.0
            st.session_state["safteyMonitorMCU"]["tempMotorMax"]    = 120.0
            st.session_state["safteyMonitorMCU"]["tempMotorFlag"]   = True
            st.session_state["safteyMonitorMCU"]["tempMCUMin"]      = 0.0
            st.session_state["safteyMonitorMCU"]["tempMCUMax"]      = 120.0
            st.session_state["safteyMonitorMCU"]["tempMCUFlag"]     = True
            st.session_state["safteyMonitorMCU"]["tempIGBTMin"]     = 0.0
            st.session_state["safteyMonitorMCU"]["tempIGBTMax"]     = 120.0
            st.session_state["safteyMonitorMCU"]["tempIGBTFlag"]    = True

        elif st.session_state["setup"]["project"] == projects[1]:
            st.session_state["safteyMonitorMCU"]["vdcMin"]          = -10.0
            st.session_state["safteyMonitorMCU"]["vdcMax"]          = 420.0
            st.session_state["safteyMonitorMCU"]["vdcFlag"]         = True
            st.session_state["safteyMonitorMCU"]["idcMin"]          = -500.0
            st.session_state["safteyMonitorMCU"]["idcMax"]          = 500.0
            st.session_state["safteyMonitorMCU"]["idcFlag"]         = True
            st.session_state["safteyMonitorMCU"]["speedMin"]        = -12000.0
            st.session_state["safteyMonitorMCU"]["speedMax"]        = 12000.0
            st.session_state["safteyMonitorMCU"]["speedFlag"]       = True
            st.session_state["safteyMonitorMCU"]["torqueMin"]       = -300.0
            st.session_state["safteyMonitorMCU"]["torqueMax"]       = 300.0
            st.session_state["safteyMonitorMCU"]["torqueFlag"]      = True
            st.session_state["safteyMonitorMCU"]["tempMotorMin"]    = 0.0
            st.session_state["safteyMonitorMCU"]["tempMotorMax"]    = 120.0
            st.session_state["safteyMonitorMCU"]["tempMotorFlag"]   = True
            st.session_state["safteyMonitorMCU"]["tempMCUMin"]      = 0.0
            st.session_state["safteyMonitorMCU"]["tempMCUMax"]      = 120.0
            st.session_state["safteyMonitorMCU"]["tempMCUFlag"]     = True
            st.session_state["safteyMonitorMCU"]["tempIGBTMin"]     = 0.0
            st.session_state["safteyMonitorMCU"]["tempIGBTMin"]     = 120.0
            st.session_state["safteyMonitorMCU"]["tempIGBTFlag"]    = True

        elif st.session_state["setup"]["project"] == projects[2]:
            st.session_state["safteyMonitorMCU"]["vdcMin"]          = -10.0
            st.session_state["safteyMonitorMCU"]["vdcMax"]          = 380.0
            st.session_state["safteyMonitorMCU"]["vdcFlag"]         = True
            st.session_state["safteyMonitorMCU"]["idcMin"]          = -500.0
            st.session_state["safteyMonitorMCU"]["idcMax"]          = 500.0
            st.session_state["safteyMonitorMCU"]["idcFlag"]         = True
            st.session_state["safteyMonitorMCU"]["speedMin"]        = -9000.0
            st.session_state["safteyMonitorMCU"]["speedMax"]        = 9000.0
            st.session_state["safteyMonitorMCU"]["speedFlag"]       = True
            st.session_state["safteyMonitorMCU"]["torqueMin"]       = -290.0
            st.session_state["safteyMonitorMCU"]["torqueMax"]       = 290.0
            st.session_state["safteyMonitorMCU"]["torqueFlag"]      = True
            st.session_state["safteyMonitorMCU"]["tempMotorMin"]    = 0.0
            st.session_state["safteyMonitorMCU"]["tempMotorMax"]    = 120.0
            st.session_state["safteyMonitorMCU"]["tempMotorFlag"]   = True
            st.session_state["safteyMonitorMCU"]["tempMCUMin"]      = 0.0
            st.session_state["safteyMonitorMCU"]["tempMCUMax"]      = 120.0
            st.session_state["safteyMonitorMCU"]["tempMCUFlag"]     = True
            st.session_state["safteyMonitorMCU"]["tempIGBTMin"]     = 0.0
            st.session_state["safteyMonitorMCU"]["tempIGBTMin"]     = 120.0
            st.session_state["safteyMonitorMCU"]["tempIGBTFlag"]    = True

    st.session_state["setup"]["project"] = clmInverter.selectbox("Project", projects, on_change = updateProjectDefaults())

    # MOTOR
    clmMotor.subheader("Motor")
    clmMotor.info("May not be needed, placeholder for now")

    # DYNO
    st.subheader("Dyno")

    clmDynoLocation, clmDynoGear = st.columns(2)

    def updateLocationDefaults():
        
        if st.session_state["setup"]["dynoLocation"] == dynoLocations[0] :
            # Setup 
            st.session_state["setup"]["dynoIP"]                     = "192.168.1.2"
            st.session_state["setup"]["dynoID"]                     = 0
            st.session_state["setup"]["dynoPort"]                   = 502
            st.session_state["setup"]["dynoGear"]                   = 3.06
            st.session_state["setup"]["ixxatMCU"]                   = "HW507598"
            st.session_state["setup"]["ixxatDCDC"]                  = "HW484965"
            st.session_state["setup"]["dcdcConfig"]                 = "Parallel"
            # Saftey Monitor Dyno Defaults
            st.session_state["safteyMonitorDyno"]["speedMin"]       = -100
            st.session_state["safteyMonitorDyno"]["speedMax"]       = 500
            st.session_state["safteyMonitorDyno"]["vdcFlag"]        = True
            st.session_state["safteyMonitorDyno"]["AccMin"]         = 0.0
            st.session_state["safteyMonitorDyno"]["AccMax"]         = 90.0
            st.session_state["safteyMonitorDyno"]["AccFlag"]        = True
            st.session_state["safteyMonitorDyno"]["tempIGBTMin"]    = 0
            st.session_state["safteyMonitorDyno"]["tempIGBTMax"]    = 120
            st.session_state["safteyMonitorDyno"]["tempIGBTFlag"]   = True
            # Saftey Monitor DCDC Defaults
            st.session_state["safteyMonitorDCDC"]["vdcMin"]         = -10
            st.session_state["safteyMonitorDCDC"]["vdcMax"]         = 500
            st.session_state["safteyMonitorDCDC"]["vdcFlag"]        = True
            st.session_state["safteyMonitorDCDC"]["tempIGBTMin"]    = 0
            st.session_state["safteyMonitorDCDC"]["tempIGBTMax"]    = 120
            st.session_state["safteyMonitorDCDC"]["tempIGBTFlag"]   = True

        elif st.session_state["setup"]["dynoLocation"] == dynoLocations[1]:

            st.session_state["setup"]["dynoIP"]                     = "192.168.1.1"
            st.session_state["setup"]["dynoID"]                     = 0
            st.session_state["setup"]["dynoPort"]                   = 502
            st.session_state["setup"]["dynoGear"]                   = 3.06
            st.session_state["setup"]["ixxatMCU"]                   = "HW507598"
            st.session_state["setup"]["ixxatDCDC"]                  = "HW484965"
            st.session_state["setup"]["dcdcConfig"]                 = "Single"
            # Saftey Monitor Dyno Defaults
            st.session_state["safteyMonitorDyno"]["speedMin"]       = -100
            st.session_state["safteyMonitorDyno"]["speedMax"]       = 500
            st.session_state["safteyMonitorDyno"]["vdcFlag"]        = True
            st.session_state["safteyMonitorDyno"]["AccMin"]         = 0.0
            st.session_state["safteyMonitorDyno"]["AccMax"]         = 90.0
            st.session_state["safteyMonitorDyno"]["AccFlag"]        = True
            st.session_state["safteyMonitorDyno"]["tempIGBTMin"]    = 0
            st.session_state["safteyMonitorDyno"]["tempIGBTMax"]    = 120
            st.session_state["safteyMonitorDyno"]["tempIGBTFlag"]   = True
            # Saftey Monitor DCDC Defaults
            st.session_state["safteyMonitorDCDC"]["vdcMin"]         = -10
            st.session_state["safteyMonitorDCDC"]["vdcMax"]         = 500
            st.session_state["safteyMonitorDCDC"]["vdcFlag"]        = True
            st.session_state["safteyMonitorDCDC"]["tempIGBTMin"]    = 0
            st.session_state["safteyMonitorDCDC"]["tempIGBTMax"]    = 120
            st.session_state["safteyMonitorDCDC"]["tempIGBTFlag"]   = True

        elif st.session_state["setup"]["dynoLocation"] == dynoLocations[2] :

            st.session_state["setup"]["dynoIP"]                     = "192.168.1.67"
            st.session_state["setup"]["dynoID"]                     = 0
            st.session_state["setup"]["dynoPort"]                   = 502
            st.session_state["setup"]["dynoGear"]                   = 3.00
            st.session_state["setup"]["ixxatMCU"]                   = "HW507598"
            st.session_state["setup"]["ixxatDCDC"]                  = "HW484965"
            st.session_state["setup"]["dcdcConfig"]                 = "Parallel"
            # Saftey Monitor Dyno Defaults
            st.session_state["safteyMonitorDyno"]["speedMin"]       = -100
            st.session_state["safteyMonitorDyno"]["speedMax"]       = 500
            st.session_state["safteyMonitorDyno"]["vdcFlag"]        = True
            st.session_state["safteyMonitorDyno"]["AccMin"]         = 0.0
            st.session_state["safteyMonitorDyno"]["AccMax"]         = 90.0
            st.session_state["safteyMonitorDyno"]["AccFlag"]        = True
            st.session_state["safteyMonitorDyno"]["tempIGBTMin"]    = 0
            st.session_state["safteyMonitorDyno"]["tempIGBTMax"]    = 120
            st.session_state["safteyMonitorDyno"]["tempIGBTFlag"]   = True
            # Saftey Monitor DCDC Defaults
            st.session_state["safteyMonitorDCDC"]["vdcMin"]         = -10
            st.session_state["safteyMonitorDCDC"]["vdcMax"]         = 500
            st.session_state["safteyMonitorDCDC"]["vdcFlag"]        = True
            st.session_state["safteyMonitorDCDC"]["tempIGBTMin"]    = 0
            st.session_state["safteyMonitorDCDC"]["tempIGBTMax"]    = 120
            st.session_state["safteyMonitorDCDC"]["tempIGBTFlag"]   = True

    st.session_state["setup"]["dynoLocation"]       = clmDynoLocation.selectbox("Location",dynoLocations, index = st.session_state["setup"]["dynoLocationIndex"], on_change = updateLocationDefaults() )
    st.session_state["setup"]["dynoLocationIndex"]  = dynoLocations.index(st.session_state["setup"]["dynoLocation"])    
        
    st.session_state["setup"]["dynoGear"]           = clmDynoGear.number_input("Gear Ratio", min_value = 0.00, max_value = 10.00, value = st.session_state["setup"]["dynoGear"], step=0.01)

    clmDynoIP, clmDynoID, clmDynoPort = st.columns(3)

    st.session_state["setup"]["dynoIP"]             = clmDynoIP.text_input("Dyno IP", value = st.session_state["setup"]["dynoIP"])
    st.session_state["setup"]["dynoID"]             = clmDynoID.number_input("Dyno ID", value = st.session_state["setup"]["dynoID"])
    st.session_state["setup"]["dynoPort"]           = clmDynoPort.number_input("Dyno Port",value =  st.session_state["setup"]["dynoPort"])

    st.subheader("CAN Hardware")

    clmIxxatMCU, clmIxxatDCDC = st.columns(2)

    st.session_state["setup"]["ixxatMCU"]           = clmIxxatMCU.text_input("MCU Ixxat HWID", value = st.session_state["setup"]["ixxatMCU"])
    st.session_state["setup"]["ixxatDCDC"]          = clmIxxatDCDC.text_input("DCDC Ixxat HWID", value = st.session_state["setup"]["ixxatDCDC"])